import mfrc522
from machine import Pin, SoftSPI
from time import sleep_ms
from neopixel import NeoPixel


sck = Pin(18, Pin.OUT)
mosi = Pin(23, Pin.OUT)
miso = Pin(19, Pin.OUT)
spi = SoftSPI(baudrate=100000, polarity=0, phase=0, sck=sck, mosi=mosi, miso=miso)

class stepCase():

    def __init__(self,spi):
        self.spi = spi
        self.r1 = Pin(16, Pin.OUT)
        self.r2 = Pin(4, Pin.OUT)
        self.r3 = Pin(2, Pin.OUT)
        self.r4 = Pin(5, Pin.OUT)
        l1 = Pin(12, Pin.OUT)
        self.light = NeoPixel(l1, 1)


    def write_data(self):
        lastRfid = ''
        scenario = int(input("Number of case: "))


        try:
            for i in range(scenario):
                step = int(input(f"Number of step in case {i+1} : "))
                j = 1
                while True:
                    rdr = mfrc522.MFRC522(self.spi, self.r1)
                    uid = ""
                    (stat, tag_type) = rdr.request(rdr.REQIDL)
                    if stat == rdr.OK:
                        (stat, raw_uid) = rdr.anticoll()
                        if stat == rdr.OK:
                            uid = ("0x%02x%02x%02x%02x" % (raw_uid[0], raw_uid[1], raw_uid[2], raw_uid[3]))
                            if lastRfid != uid:
                                temp = lastRfid
                                lastRfid = uid
                            else:
                                continue
                            if rdr.select_tag(raw_uid) == rdr.OK:
                                key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
                                if rdr.auth(rdr.AUTHENT1A, 8, key, raw_uid) == rdr.OK:
                                    stat = rdr.write(8, bytearray([i+1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,j]).decode('utf-8').encode('utf-8'))
                                    j += 1
                                    rdr.stop_crypto1()
                                    if stat == rdr.OK:
                                        print("Data written to card")
                                    else:
                                        lastRfid = temp
                                        print("Failed to write data to card")
                                    sleep_ms(100)
                                else:
                                    lastRfid = temp
                                    print("Auth Error")
                            else:
                                lastRfid = temp
                                print("Error selecting tag")
                    if j-1 == step:
                        break
        except KeyboardInterrupt:
            return


    def read_data(self,sda):
        try:
            while True:
                rdr = mfrc522.MFRC522(self.spi, sda)
                (stat, tag_type) = rdr.request(rdr.REQIDL)
                if stat == rdr.OK:
                    (stat, raw_uid) = rdr.anticoll()
                    if stat == rdr.OK:
                        if rdr.select_tag(raw_uid) == rdr.OK:
                            key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
                            if rdr.auth(rdr.AUTHENT1A, 8, key, raw_uid) == rdr.OK:
                                value = rdr.read(8)
                                rdr.stop_crypto1()
                                return (value[0],value[-1])
                            else:
                                print("Auth Error")
                        else:
                            print("Error selecting tag")
            
        except KeyboardInterrupt:
            return

    def validate(self):
        def indicator(stepValue):
            for _ in range(stepValue[0]):
                self.light[0] = (60,10,70)
                self.light.write()
                sleep_ms(1000)
                self.light[0] = (0,0,0)
                self.light.write()


            for _ in range(stepValue[1]):
                self.light[0] = (0,0,70)
                self.light.write()
                sleep_ms(500)
                self.light[0] = (0,0,0)
                self.light.write()

        step1 = self.read_data(self.r1)
        indicator(stepValue=step1)
        step2 = self.read_data(self.r2)
        indicator(stepValue=step2)
        step3 = self.read_data(self.r3)
        indicator(stepValue=step3)
        step4 = self.read_data(self.r4)
        indicator(stepValue=step4)


        if step1[0] == step2[0] == step3[0] == step4[0] and (step1[1] == 1 and step2[1] == 2 and step3[1] == 3 and step4[1] == 4):
            self.light[0] = (0,100,0)
            self.light.write()
        else:
            self.light[0] = (100,0,0)
            self.light.write()

program = stepCase(spi)


program.light[0] = (0, 0, 0)
program.light.write()

sleep_ms(3000)

rdr = mfrc522.MFRC522(spi,Pin(2, Pin.OUT))

sleep_ms(3000)

rdr = mfrc522.MFRC522(spi,Pin(16, Pin.OUT))
sleep_ms(3000)

rdr = mfrc522.MFRC522(spi,Pin(4, Pin.OUT))

sleep_ms(3000)
rdr = mfrc522.MFRC522(spi,Pin(5, Pin.OUT))

program.light[0] = (100,50,255)
program.light.write()
try:
    while True:
        program.validate()
except:
    pass
