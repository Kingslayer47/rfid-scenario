# RFID Scenario

## About the project

The program is for ESP-32 micro-controller , to configure them to encourage visual step-by-step learning in children, the cards can be programmed according to the scenario and steps, and then later on validated via the same.

## GPIO pins

I used the following pins for my setup:

| Signal   | GPIO ESP32   | Note                                 |
| ---------| ------------ | ------------------------------------ |
| sck      | 18           |                                      |
| mosi     | 23           |                                      |
| miso     | 19           |                                      |
| cs       | 5            |Reader 1                              |
| cs       | 2            |Reader 2                              |
| ARGB     | 4            |                                      |

## Usage

put both of the files in root of your esp-32 micro-controller

# Write data 
By using ``program.write_data()`` you can write to RFID cards and tags.

By default the program, will pick ``Reader 1`` at ``Pin 5`` to write data,
It'll ask you to put in scenario and Step for each scenario.

# Read data
By using ``program.read_data()`` you can check the scenario and step ,

you need to pass the RFID reader you want to read_data from , for example ``program.read_data(program.r1)``

# Validate data

By using ``program.validate()`` you can check the order of scenario and step.

It'll take in inputs from the number of RFID scanners present in your circuit in order and validate them.

If the order is correct the ARGB LED will turn Green else it'll turn Red.

## Special Mention

Thanks to ``https://github.com/Tasm-Devil/micropython-mfrc522-esp32/tree/master`` for providing the code with mfrc522 port for ESP 32.